package com.epam.task3;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Date;
import java.io.*;

public class BuffUnbuffReadWwrite {

    private byte[] read(InputStream is) throws IOException {
        byte[] buffer = new byte[is.available()];
        int data;
        int i = 0;
        while ((data = is.read()) != -1) {
            buffer[i++] = (byte) data;
        }
        return buffer;
    }

    public void bufferedReadWrite() throws IOException {
        int count = 0;
        try (
                BufferedInputStream in =
                        new BufferedInputStream(
                                new FileInputStream("E:\\1.pdf"));
                BufferedOutputStream on =
                        new BufferedOutputStream(
                                new FileOutputStream("E:\\1_copy.pdf"))) {

            Date start = new Date();
            byte[] buffer = read(in);
            Date end = new Date();
            System.out.println("buffered reading time(ms): "
                    + (end.getTime() - start.getTime()));

            System.out.println("Test buffered writer");
            start = new Date();
            on.write(buffer);
            end = new Date();
            System.out.println("buffered writing time(ms): "
                    + (end.getTime() - start.getTime()));
            System.out.println("count = " + count);
        }
    }

    public static void main(String[] args) throws IOException {
        BuffUnbuffReadWwrite buffUnbuffReadWwrite = new BuffUnbuffReadWwrite();
        buffUnbuffReadWwrite.bufferedReadWrite();
    }
}
