package com.epam.task2;

import java.io.Serializable;
import java.util.Arrays;

public class Ship implements  Serializable{
    private String nameShip;
    private int distance;
    private int numberOfPeople;
    private Droid[] droids;

    public Ship(String nameShip, int distance, int numberOfPeople, Droid[] droids) {
        this.nameShip = nameShip;
        this.distance = distance;
        this.numberOfPeople = numberOfPeople;
        this.droids = droids;
    }

    public String getNameShip() {
        return nameShip;
    }

    public void setNameShip(String nameShip) {
        this.nameShip = nameShip;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public int getNumberOfPeople() {
        return numberOfPeople;
    }

    public void setNumberOfPeople(int numberOfPeople) {
        this.numberOfPeople = numberOfPeople;
    }

    public Droid[] getDroids() {
        return droids;
    }

    public void setDroids(Droid[] droids) {
        this.droids = droids;
    }

    @Override
    public String toString() {
        return "Ship{" + super.toString() +
                "nameShip='" + nameShip + '\'' +
                ", distance=" + distance +
                ", numberOfPeople=" + numberOfPeople +
                ", droids=" + Arrays.toString(droids) +
                '}';
    }

}


