package com.epam.task2;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Application {
    public static void main(String[] args) {
        Droid[] droids ={
                new Droid("R1",400,"1"),
                new Droid("R1",300,"2"),
                new Droid("R1",200,"3"),
        };
        Ship ship =(
                new Ship("Titanic",1000,500,droids)
        );
        try (ObjectOutputStream out =
                     new ObjectOutputStream(
                             new FileOutputStream(
                                     "E:\\ship.out"))) {
            out.writeObject(ship);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        try (ObjectInputStream in =
                     new ObjectInputStream(
                             new FileInputStream(
                                     "E:\\ship.out"))) {
            Ship desShip = (Ship) in.readObject();
            System.out.println(desShip);
        } catch (IOException | ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
}
