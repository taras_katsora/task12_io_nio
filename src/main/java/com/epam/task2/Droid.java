package com.epam.task2;

import java.io.Serializable;

public class Droid implements  Serializable{
    private int id = count++;
    private String modelName;
    private int flightRange;
    private transient String selfDestructCode;
    private static int count = 0;

    public Droid( String modelName, int flightRange, String selfDestructCode) {
        this.modelName = modelName;
        this.flightRange = flightRange;
        this.selfDestructCode = selfDestructCode;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public int getFlightRange() {
        return flightRange;
    }

    public void setFlightRange(int flightRange) {
        this.flightRange = flightRange;
    }

    public String getSelfDestructCode() {
        return selfDestructCode;
    }

    public void setSelfDestructCode(String selfDestructCode) {
        this.selfDestructCode = selfDestructCode;
    }

    @Override
    public String toString() {
        return "Droid{" + super.toString() +
                "modelName='" + modelName + '\'' +
                ", flightRange=" + flightRange +
                ", selfDestructCode='" + selfDestructCode + '\'' +
                '}';
    }
}
