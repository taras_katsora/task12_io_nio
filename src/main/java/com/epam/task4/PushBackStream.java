package com.epam.task4;

import java.io.*;


public class PushBackStream {
    public static void main(String arg[]) throws IOException
    {
        PrintWriter pw = new PrintWriter(System.out, true);
        String str = "Hey geeks ";
        byte b[] = str.getBytes();
        ByteArrayInputStream input = new ByteArrayInputStream(b);
        PushbackInputStream push = new PushbackInputStream(
                new FileInputStream("E:\\123.txt"), 8);
        int data = input.read();
        push.unread(data);

        // checking no. of bytes available
        pw.println("availble bytes: " + push.available());

        // checking if mark is supported
        pw.println("mark supported? :" + push.markSupported());

        pw.close();
    }
}
